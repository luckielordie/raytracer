﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Engine.Mathematics;

namespace RayTracer
{
    //Ray Tracer from here [http://fabiensanglard.net/rayTracing_back_of_business_card/]
    class Program
    {
        static int[] G = new int[]{247570,280596,280600,249748,18578,18577,231184,16,16};
        static Random random;
        //0 miss Sky
        //1 miss floor
        //2 sphere hit
        static int Tracer(Vector3 origin, Vector3 direction, ref float distanceOfIntersection, ref Vector3 halfVectorRayBounce)
        {
            int m = 0;
            float p = -origin.Z / direction.Z;

            halfVectorRayBounce = new Vector3();

            if (0.01 < p)
            {
                distanceOfIntersection = p;
                halfVectorRayBounce = new Vector3(0.0f, 0.0f, 1.0f);
                m = 1;
            }


            for (int k = 18; k >= 0; k--)
            {
                for (int j = 8; j >= 0; j--)
                {
                    if ((G[j] & (1 << k)) != 0)
                    {
                        Vector3 p2 = origin + new Vector3(-k, 0.0f, -j - 4);

                        float b = p2.Dot(direction);
                        float c = p2.Dot(p2) - 1;
                        float q = (b * b) - c;

                        if (q > 0)
                        {
                            float s = (float)(-b - Math.Sqrt(q));

                            if (s < distanceOfIntersection && s > 0.01)
                            {
                                distanceOfIntersection = s;
                                halfVectorRayBounce = Vector3.Normalise(p2 + (direction * distanceOfIntersection));
                                m = 2;
                            }
                        }
                    }
                }
            }

            return m;

        }

        static Vector3 Sample(Vector3 origin, Vector3 direction)
        {
            float distanceOfIntersection = float.MaxValue;
            Vector3 halfVectorRayBounce = new Vector3();

            int m = Tracer(origin, direction, ref distanceOfIntersection, ref halfVectorRayBounce);

            //No hit gen sky
            if (m == 0)
            {
                Vector3 outColour = new Vector3(0.7f, 0.6f, 1.0f) * Math.Pow(1 - direction.Z, 4);
                return outColour;
            }

            Vector3 intersection = origin + (direction * distanceOfIntersection);
            Vector3 directionToLight = Vector3.Normalise(new Vector3(9 + (float)random.NextDouble(), 9 + (float)random.NextDouble(), 16) + (intersection * -1));
            Vector3 halfVector = direction + halfVectorRayBounce * (halfVectorRayBounce.Dot(direction * -2));

            //lambertian factor
            float b = directionToLight.Dot(halfVectorRayBounce);

            //calculate illumination factor
            if (b < 0 || Tracer(intersection, directionToLight, ref distanceOfIntersection, ref halfVectorRayBounce) != 0) b = 0;

            float p = (float)Math.Pow(directionToLight.Dot(halfVector * b), 99);

            if(m == 1) //No hit gen floor
            {
                Vector3 floorColour = new Vector3();
                intersection *= 0.2f;
                if((((int)(Math.Ceiling(intersection.X) + Math.Ceiling(intersection.Y))) & 1) != 0)
                {
                    floorColour =  new Vector3(3.0f, 1.0f, 1.0f);
                }else
                {
                    floorColour = new Vector3(3.0f, 3.0f, 3.0f);
                }
                floorColour *= (b * 0.2f + 0.1f);

                return floorColour; 
            }

            Vector3 outColour2 = new Vector3(p, p, p) + Sample(intersection, halfVector) * 0.5f; //attenuate colour by 50%
            return outColour2;
        }

        static void Main(string[] args)
        {
            int width = 1920;
            int height = 1080;
            int raysPerPixel = 1024;
            Console.WriteLine("Starting...");

            random = new Random();
            using (Bitmap image = new Bitmap(width, height))
            {
                //Create Camera
                //Vector3 cameraDirection = new Vector3(0.0f, 0.0f, 1.0f);
                //Vector3 cameraUp = new Vector3(0.0f, 1.0f, 0.0f);
                //Vector3 cameraRight = cameraUp.Cross(cameraDirection);
                //Vector3 c = (cameraUp + cameraRight) * -256 + cameraDirection;

                Vector3 cameraDirection = Vector3.Normalise(new Vector3(-6, -16, 0));
                Vector3 cameraRight = Vector3.Normalise(new Vector3(0, 0, 1).Cross(cameraDirection)) * (1f/height);
                Vector3 cameraUp = Vector3.Normalise(cameraDirection.Cross(cameraRight)) * (1f/height);
                Vector3 c = cameraRight * -(width/2) + cameraUp* -(height/2) + cameraDirection;

                //Vector3.Normalise(cameraDirection);
                //Vector3.Normalise(cameraUp);
                //Vector3.Normalise(cameraRight);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        Vector3 pixelColour = new Vector3(0, 0, 0);

                        Console.WriteLine("Pixel {0}/{1}", (y * width) + x, width * height);
                        for (int ray = 0; ray < raysPerPixel; ray++)
                        {

                            Vector3 dof = new Vector3();
                            dof = cameraRight * ((float)random.NextDouble() - 0.5f) * 99 + cameraUp * ((float)random.NextDouble() - 0.5f) * 99;

                            Vector3 direction = Vector3.Normalise(dof * -1 + (cameraRight * ((float)random.NextDouble() + x) + cameraUp * (y + (float)random.NextDouble()) + c) * 16);
                            //pixelColour = Sample
                            pixelColour += Sample(new Vector3(17.0f, 16.0f, 8.0f) + dof, direction);
                        }
                        pixelColour = pixelColour / raysPerPixel;
                        Color colour = Vector3.ToColor(pixelColour);
                            
                        image.SetPixel((width - 1) -x,(height - 1)- y, colour);                        
                    }
                }

                string path = ((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds).ToString() + ".png";

                image.Save(path);
                System.Diagnostics.Process.Start(path);
            }
        }
    }
}
